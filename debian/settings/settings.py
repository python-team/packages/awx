# Import all so that extra_settings works properly
from django_auth_ldap.config import *

# work around initialization issue when calling migrate
import os, base64
if os.path.exists('/etc/tower/SECRET_KEY'):
    with open('/etc/tower/SECRET_KEY', 'rb') as f:
        SECRET_KEY = f.read().strip()
else:
    SECRET_KEY = base64.encodebytes(os.urandom(32)).decode().rstrip()

if os.path.exists('/etc/tower/BROADCAST_WEBSOCKET_SECRET'):
    with open('/etc/tower/BROADCAST_WEBSOCKET_SECRET', 'rb') as f:
        BROADCAST_WEBSOCKET_SECRET = f.read().strip()
else:
    BROADCAST_WEBSOCKET_SECRET = base64.encodebytes(os.urandom(32)).decode().rstrip()


