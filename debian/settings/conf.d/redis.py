BROKER_URL = "redis://127.0.0.1:6379"
CACHES['default']['LOCATION'] = BROKER_URL
# https://github.com/django/channels_redis/issues/332
CHANNEL_LAYERS["default"]["BACKEND"] = "channels_redis.pubsub.RedisPubSubChannelLayer"
CHANNEL_LAYERS["default"]["CONFIG"]["hosts"] = [BROKER_URL]


